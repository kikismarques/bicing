import './App.css'
import Pagina1 from './Componentes/Pagina1';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from "react-bootstrap";


function App() {
  return (
    <div className='App'>
      <Pagina1 />
      <Container />
    </div>
  )
}

export default App;
