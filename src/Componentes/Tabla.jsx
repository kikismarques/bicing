import { Table } from "react-bootstrap";

function Tabla(props){

    let cabecera = props.head.map((elemento, i ) => {
        return <th key={i}>{elemento}</th>
   })

   return (
       <div>
          <Table bordered hover>
              <thead>
                  <tr>
                       {cabecera}                      
                  </tr>
              </thead>
              <tbody>
                  {props.body}
              </tbody>
          </Table>
       </div>
   );
}


export default Tabla