import { useState, useEffect } from "react";
import { Container, Button } from "react-bootstrap";

import Tabla from "./Tabla";
import Map from "./Map";


function Pagina1() {

    const [datos, setDatos] = useState([])
    const [filter, setFilter] = useState("")

    const header = ["Estacion", "Bicis disponibles", "Slots Libres", "Latitud", "Longitud"]

    const fetchResult = async () => {
        const response = await fetch("https://api.citybik.es/v2/networks/bicing");
        const result = await response.json();
        return result;
    }

    const cargaDatos = () => {
        fetchResult()
            .then(result => {
                if (filter !== "") {
                    setDatos((result.network.stations).filter(bicing => bicing.free_bikes >= parseInt(filter)))
                } else {
                    setDatos(result.network.stations)
                }
            })
            .catch((e) => console.log("Error: " + e))
    }


    useEffect(() => {
        cargaDatos();
    }, [filter])


    function fillRows(bicing, i) {
        return (
            < tr key={i} >
                <td>{bicing.name}</td>
                <td>{bicing.free_bikes}</td>
                <td>{bicing.empty_slots}</td>
                <td>{bicing.latitude}</td>
                <td>{bicing.longitude}</td>
            </tr >
        );
    }

    let rows = datos.map((bicing, i) => fillRows(bicing, i))

    return (
        <div>
            <h1>BICIS</h1>
            <Container>
                <Map sites={datos} />
                <div className="botonera">
                    <Button onClick={() => cargaDatos}>Carga bicis</Button>
                    <input className="filter" type="text" onInput={(e) => { setFilter(e.target.value) }} />
                </div>
                <Tabla head={header} body={rows} />
            </Container>
        </div>

    );
}






export default Pagina1;